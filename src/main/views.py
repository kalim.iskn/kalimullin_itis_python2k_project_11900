from django.shortcuts import render

from tracks.services import get_popular_tracks_list


def main_page(request):
    tracks = get_popular_tracks_list()
    context = {
        "tracks": tracks
    }
    return render(request, "main/main_page.html", context)


def get_404_page(request, exception):
    return render(request, "main/errors/404.html")


def get_500_page(request, *args, **kwargs):
    return render(request, "main/errors/500.html")
