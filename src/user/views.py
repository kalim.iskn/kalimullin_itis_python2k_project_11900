from django.contrib.auth import get_user_model, authenticate, login
from django.contrib.auth.decorators import login_required
from django.contrib.messages.views import SuccessMessageMixin
from django.http import Http404
from django.shortcuts import render, redirect
from django.urls import reverse, reverse_lazy
from django.utils.decorators import method_decorator
from django.views import View
from django.views.generic import UpdateView

from tracks.services import get_user_tracks_list
from user.forms import RegistrationForm
from user.services import get_artists_list

User = get_user_model()


def sign_up(request):
    if request.method == "POST":
        form = RegistrationForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            user = authenticate(request, username=form.cleaned_data['username'],
                                password=form.cleaned_data['password1'])
            login(request, user)
            return redirect(reverse('my_profile'))
    else:
        form = RegistrationForm()
    return render(request, "registration/registration.html", {"form": form})


def get_profile(request, username):
    # TODO redirect to 404 page
    try:
        info = User.objects.get(username=username)
    except User.DoesNotExist:
        raise Http404("User does not exist")

    tracks = get_user_tracks_list(info.pk)

    context = {
        "url_username": username,
        "info": info,
        "tracks": tracks
    }

    return render(request, "user/profile.html", context)


@login_required()
def go_to_my_profile(request):
    page = reverse('artist-profile', args=(request.user.username,))
    return redirect(page)


class EditProfileView(SuccessMessageMixin, UpdateView):
    template_name = 'user/edit-profile.html'
    model = User
    fields = ["username", "pseudonym", "bio", "avatar", "cover"]
    success_url = reverse_lazy('edit-profile')
    success_message = "Your profile has been successfully updated"

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get_object(self, queryset=None):
        return self.model.objects.get(username=self.request.user.username)


class ArtistsView(View):
    def get(self, request):
        artists = get_artists_list()
        context = {
            "artists": artists
        }
        return render(request, "user/artists.html", context)
