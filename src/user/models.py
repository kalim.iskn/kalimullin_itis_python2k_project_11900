from django.contrib.auth.models import AbstractUser
from django.db import models


class CustomUser(AbstractUser):
    email = models.EmailField(unique=True)
    avatar = models.ImageField(upload_to="avatars")
    cover = models.ImageField(upload_to="covers", blank=True)
    bio = models.TextField(max_length=500, blank=True)
    is_verified = models.BooleanField(default=False)
    pseudonym = models.CharField(max_length=60)

    def __str__(self):
        return self.pseudonym + " - @" + self.username
