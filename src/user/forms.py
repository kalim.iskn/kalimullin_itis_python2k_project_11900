from django import forms
from django.contrib.auth.forms import UserCreationForm

from user.models import CustomUser


class RegistrationForm(UserCreationForm):
    email = forms.EmailField()
    bio = forms.CharField(widget=forms.Textarea, label="Short info about yourself", required=False)

    class Meta:
        model = CustomUser
        fields = ["username", "pseudonym", "email", "bio", "avatar", "cover", "password1", "password2"]
