from django.contrib.auth import views as auth_views
from django.urls import path

from . import views

urlpatterns = [
    path("artist/<str:username>", views.get_profile, name='artist-profile'),
    path("my-profile/", views.go_to_my_profile, name='my_profile'),
    path("edit-profile/", views.EditProfileView.as_view(), name='edit-profile'),
    path("artists", views.ArtistsView.as_view(), name='artists'),
    path('signin/', auth_views.LoginView.as_view(), name='signin'),
    path('logout/', auth_views.LogoutView.as_view(), name='logout'),
    path("signup/", views.sign_up, name='signup'),
]
