from django.contrib.auth import get_user_model

User = get_user_model()


def get_artists_list():
    return User.objects.all().order_by("-is_verified", "-date_joined").values()
