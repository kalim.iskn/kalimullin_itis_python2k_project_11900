from django.contrib.auth import get_user_model
from django.db import models
from django.template.defaultfilters import date

from main.models import BaseModel
from tracks.helpers.slugify import unique_slugify

User = get_user_model()


class Track(BaseModel):
    slug = models.SlugField(max_length=80, unique=True, null=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="tracks")
    audio = models.FileField(upload_to="tracks")
    name = models.CharField(max_length=70)
    description = models.TextField(max_length=1000, blank=True)
    poster = models.ImageField(upload_to="posters")
    number_of_listens = models.IntegerField(default=0)
    number_of_likes = models.IntegerField(default=0)

    def save(self, **kwargs):
        unique_slugify(self, self.name)
        super(Track, self).save(**kwargs)

    def get_formatted_created_data(self):
        return date(self.created_at, "d F, Y")

    def __str__(self):
        return self.name


class Listener(models.Model):
    track = models.ForeignKey(Track, on_delete=models.CASCADE)
    ip = models.GenericIPAddressField()


class Like(models.Model):
    track = models.ForeignKey(Track, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
