from django.contrib.auth.decorators import login_required
from django.contrib.messages.views import SuccessMessageMixin
from django.http import HttpResponse, Http404
from django.shortcuts import render
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views import View
from django.views.generic import CreateView, DetailView, UpdateView, DeleteView

from tracks.forms import AddTrackForm
from tracks.helpers.audio_response import get_audio_response
from tracks.helpers.get_ip_address import get_ip
from tracks.services import *


def get_audio(request, filename):
    return get_audio_response(filename)


def add_listener(request, track_id):
    ip = get_ip(request)
    is_created = add_listener_to_track(track_id, ip)
    return HttpResponse(str(is_created))


def like_track(request, track_id):
    if not request.user.is_authenticated:
        raise Http404()

    is_added = add_like_to_track(track_id, request.user)
    return HttpResponse(str(is_added))


@login_required()
def get_my_tracks(request):
    tracks = get_user_tracks_list(request.user.pk)
    context = {
        "tracks": tracks
    }
    return render(request, "tracks/my_tracks.html", context)


class AddTrackView(SuccessMessageMixin, CreateView):
    model = Track
    form_class = AddTrackForm
    template_name = "tracks/new_track.html"
    success_url = reverse_lazy('new_track')
    success_message = "Track has been successfully added"

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super(AddTrackView, self).form_valid(form)


class EditTrackView(SuccessMessageMixin, UpdateView):
    model = Track
    template_name = 'tracks/edit_track.html'
    fields = ['name', 'description', 'poster']
    success_url = reverse_lazy('my_tracks')
    success_message = "Your track has been successfully updated"

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        obj = self.get_object()
        if obj.user != self.request.user:
            raise Http404("You are not allowed to edit this track")
        return super().dispatch(*args, **kwargs)


class DeleteTrackView(SuccessMessageMixin, DeleteView):
    model = Track
    success_url = reverse_lazy('my_tracks')

    success_message = "Your track has been successfully deleted"

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        obj = self.get_object()
        if obj.user != self.request.user:
            raise Http404("You are not allowed to delete this track")
        return super().dispatch(*args, **kwargs)

    def get(self, *args, **kwargs):
        return self.post(*args, **kwargs)


class TrackInfoView(DetailView):
    model = Track
    template_name = "tracks/track_page.html"

    def get_context_data(self, **kwargs):
        context = super(TrackInfoView, self).get_context_data(**kwargs)
        if self.request.user.is_authenticated:
            context['isLiked'] = int(is_liked(context['object'].id, self.request.user))
        else:
            context['isLiked'] = 1
        return context


class TracksView(View):
    def get(self, request):
        tracks = get_all_tracks_list()
        context = {
            "tracks": tracks
        }
        return render(request, 'tracks/tracks.html', context)
