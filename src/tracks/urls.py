from django.urls import path

from . import views

urlpatterns = [
    path("track/<slug:slug>", views.TrackInfoView.as_view(), name="track_page"),
    path("new-track/", views.AddTrackView.as_view(), name="new_track"),
    path("edit-track/<slug:slug>/", views.EditTrackView.as_view(), name="edit_track"),
    path("delete-track/<slug:slug>/", views.DeleteTrackView.as_view(), name="delete_track"),
    path("my-tracks", views.get_my_tracks, name="my_tracks"),
    path("tracks", views.TracksView.as_view(), name="tracks"),
    path("tracks/add-listener/<int:track_id>", views.add_listener, name="add_listener_track"),
    path("tracks/like/<int:track_id>", views.like_track, name="like_track"),
    path("media/tracks/<str:filename>", views.get_audio, name='get_audio'),
]
