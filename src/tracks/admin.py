from django.contrib import admin

from tracks.forms import TrackAdminForm
from tracks.models import Track


class TrackAdmin(admin.ModelAdmin):
    form = TrackAdminForm
    list_display = ('name', 'user', 'slug', 'number_of_listens')
    search_fields = ('name', 'slug')
    readonly_fields = ('slug',)

    def get_form(self, request, obj=None, **kwargs):
        if not obj:
            self.form = self.form
        else:
            self.form = self.form

        return super(TrackAdmin, self).get_form(request, obj, **kwargs)


admin.site.register(Track, TrackAdmin)
