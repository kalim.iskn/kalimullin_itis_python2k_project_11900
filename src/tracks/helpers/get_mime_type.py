import magic


def get_mime_type(file):
    initial_pos = file.tell()
    file.seek(0)
    mime_type = magic.from_buffer(file.read(), mime=True)
    file.seek(initial_pos)
    return mime_type
