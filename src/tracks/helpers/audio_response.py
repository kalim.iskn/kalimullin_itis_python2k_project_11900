import os

from django.http import HttpResponse
from django.utils.encoding import smart_str

from mysound.settings import MEDIA_ROOT


def get_audio_response(filename):
    path_to_file = os.path.join(MEDIA_ROOT, "tracks", filename)
    response = HttpResponse(content_type='audio/mpeg')
    f = open(path_to_file, "rb")
    response.write(f.read())
    response['Content-Disposition'] = 'inline; filename=%s' % smart_str(filename)
    response['Accept-Ranges'] = 'bytes'
    response['X-Sendfile'] = smart_str(path_to_file)
    return response
