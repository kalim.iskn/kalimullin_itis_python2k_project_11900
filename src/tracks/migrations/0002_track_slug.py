# Generated by Django 3.1.7 on 2021-03-14 15:17

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tracks', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='track',
            name='slug',
            field=models.SlugField(default='', max_length=80, unique=True),
        ),
    ]
