from django.db.models import F

from tracks.models import Track
from tracks.models.tracks import Listener, Like


def get_user_tracks_list(user):
    return Track.objects.filter(user=user).order_by("-created_at").values()


def get_all_tracks_list():
    return Track.objects.select_related("user").all().order_by("-created_at").values(
        "id", "audio", "user__pseudonym", "name", "slug", "poster"
    )


def get_popular_tracks_list():
    return Track.objects.select_related("user").order_by("-number_of_listens", "-created_at").values(
        "id", "audio", "user__pseudonym", "name", "slug", "poster"
    ).all()[:8]


def add_listener_to_track(track_id, ip):
    created = False
    try:
        track = Track.objects.get(id=track_id)
        if not Listener.objects.filter(track=track_id, ip=ip).exists():
            listener = Listener()
            listener.track_id = track_id
            listener.ip = ip
            listener.save()
            track.number_of_listens = track.number_of_listens + 1
            track.save()
            created = True
    except Track.DoesNotExist:
        created = False
    finally:
        return created


def add_like_to_track(track_id, user):
    (track, created) = Like.objects.get_or_create(track_id=track_id, user=user)
    if created:
        Track.objects.filter(id=track_id).update(number_of_likes=F('number_of_likes') + 1)
    return created


def is_liked(track_id, user):
    return Like.objects.filter(track_id=track_id, user=user).exists()
