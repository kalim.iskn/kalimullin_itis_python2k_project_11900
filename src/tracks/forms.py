from django import forms

from mysound.settings import MAX_AUDIO_FILE_SIZE, MAX_AUDIO_FILE_SIZE_MEGABYTES
from tracks.helpers.get_mime_type import get_mime_type
from tracks.models import Track


class TrackFormBase(forms.ModelForm):
    class Meta:
        model = Track
        fields = []

    error_messages = {
        'audio_type': 'Audio type is not supported',
        'audio_parameters': 'The audio must have mp3 extension and weigh no more than ' +
                            str(MAX_AUDIO_FILE_SIZE_MEGABYTES) + ' megabytes',
        'file_error': 'There was an error loading audio recording'
    }

    def clean_audio(self):
        file = self.cleaned_data['audio']
        if file:
            if len(file.name.split('.')) == 1:
                raise forms.ValidationError(self.error_messages['audio_type'], code="audio_type")
            if file.size > MAX_AUDIO_FILE_SIZE:
                raise forms.ValidationError(self.error_messages['audio_parameters'], code="audio_parameters")

            file_type = get_mime_type(file)

            if file_type == "audio/mpeg":
                return file
            else:
                raise forms.ValidationError(self.error_messages['audio_parameters'], code="audio_parameters")

        else:
            raise forms.ValidationError(self.error_messages['file_error'], code='file_error')


class AddTrackForm(TrackFormBase):
    class Meta(TrackFormBase.Meta):
        fields = ['name', 'description', 'poster', 'audio']


class TrackAdminForm(TrackFormBase):
    class Meta(TrackFormBase.Meta):
        fields = ['name', 'description', 'poster', 'audio', 'user']
