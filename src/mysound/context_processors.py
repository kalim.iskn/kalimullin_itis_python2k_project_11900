def is_pjax_request(request):
    is_pjax = 'X-Pjax' in request.headers.keys()
    return {'is_pjax': is_pjax}
